#!/bin/sh -eu

cd $(dirname $0)

BE_SRC_ARCHIVE=$(ls sane-backends-*-*.tar.gz)
FE_SRC_ARCHIVE=$(ls sane-frontends-*-g*.tar.gz)
BE_GIT_ARCHIVE=$(ls sane-backends-git*.tar.gz)
FE_GIT_ARCHIVE=$(ls sane-frontends-git*.tar.gz)

BE_SRC_DATE=$(date "+%F %T" -d "$(stat -c %y $BE_SRC_ARCHIVE | sed 's/\..*$//')")
FE_SRC_DATE=$(date "+%F %T" -d "$(stat -c %y $FE_SRC_ARCHIVE | sed 's/\..*$//')")
BE_GIT_DATE=$(date "+%F %T" -d "$(stat -c %y $BE_GIT_ARCHIVE | sed 's/\..*$//')")
FE_GIT_DATE=$(date "+%F %T" -d "$(stat -c %y $FE_GIT_ARCHIVE | sed 's/\..*$//')")

BE_SRC_SIZE=$(stat -c %s $BE_SRC_ARCHIVE)
FE_SRC_SIZE=$(stat -c %s $FE_SRC_ARCHIVE)
BE_GIT_SIZE=$(stat -c %s $BE_GIT_ARCHIVE)
FE_GIT_SIZE=$(stat -c %s $FE_GIT_ARCHIVE)

cat <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /snapshots</title>
 </head>
 <body>
<h1>Index of /snapshots</h1>
<table><tr><th><img src="../icons/blank.gif" alt="[ICO]"></th><th>Name</th><th>Last modified</th><th>Size</th><th>Description</th></tr><tr><th colspan="5"><hr></th></tr>
<tr><td valign="top"><img src="../icons/back.gif" alt="[DIR]"></td><td><a href="../">Parent Directory</a></td><td>&nbsp;</td><td align="right">  - </td><td>&nbsp;</td></tr>
<tr><td valign="top"><img src="../icons/compressed.gif" alt="[   ]"></td><td><a href="$BE_SRC_ARCHIVE">$BE_SRC_ARCHIVE</a></td><td align="right">$BE_SRC_DATE  </td><td align="right">$BE_SRC_SIZE</td><td>&nbsp;</td></tr>
<tr><td valign="top"><img src="../icons/compressed.gif" alt="[   ]"></td><td><a href="$BE_GIT_ARCHIVE">$BE_GIT_ARCHIVE</a></td><td align="right">$BE_GIT_DATE  </td><td align="right">$BE_GIT_SIZE</td><td>&nbsp;</td></tr>
<tr><td valign="top"><img src="../icons/compressed.gif" alt="[   ]"></td><td><a href="$FE_SRC_ARCHIVE">$FE_SRC_ARCHIVE</a></td><td align="right">$FE_SRC_DATE  </td><td align="right">$FE_SRC_SIZE</td><td>&nbsp;</td></tr>
<tr><td valign="top"><img src="../icons/compressed.gif" alt="[   ]"></td><td><a href="$FE_GIT_ARCHIVE">$FE_GIT_ARCHIVE</a></td><td align="right">$FE_GIT_DATE  </td><td align="right">$FE_GIT_SIZE</td><td>&nbsp;</td></tr>
<tr><th colspan="5"><hr></th></tr>
</table>
</body></html>
EOF

rm $(basename $0)
